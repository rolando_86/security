package com.spring.security.spring.security.Servicio;

import com.spring.security.spring.security.Entidad.Libro;
import com.spring.security.spring.security.Repositorio.LibroRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LibroServicio {

    private final LibroRepositorio libroRepositorio;

    @Autowired
    public LibroServicio(LibroRepositorio libroRepositorio) {
        this.libroRepositorio = libroRepositorio;
    }

    public List<Libro> obtenerLibros() {
        return libroRepositorio.findAll();
    }

    public Libro crearLibro(Libro libro) {
        return libroRepositorio.save(libro);
    }
}
