package com.spring.security.spring.security.Servicio;

import com.spring.security.spring.security.Entidad.Rol;
import com.spring.security.spring.security.Entidad.Usuario;
import com.spring.security.spring.security.Repositorio.UsuarioRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UsuarioDetalleService implements UserDetailsService {

    @Autowired
    private UsuarioRepositorio usuarioRepositorio;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<Usuario> usuarioOptional = usuarioRepositorio.findByNombreUsuario(s);
        if (!usuarioOptional.isPresent()) {
            throw new UsernameNotFoundException("Usuario no encontrado");
        }
        Usuario usuario = usuarioOptional.get();
        Set<Rol> rolesUsuario = usuario.getRoles();
        List<GrantedAuthority> roles = new ArrayList<>();
        for (Rol rol : rolesUsuario) {
            roles.add(new SimpleGrantedAuthority(rol.getNombreRol()));
        }
        UserDetails userDetails = new User(usuario.getNombreUsuario(), usuario.getContrasena(), roles);
        return userDetails;
    }
}
