package com.spring.security.spring.security.Controlador;

import com.spring.security.spring.security.Entidad.Libro;
import com.spring.security.spring.security.Servicio.LibroServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/libro")
public class LibroControlador {
    private final LibroServicio libroServicio;

    @Autowired
    public LibroControlador(LibroServicio libroServicio) {
        this.libroServicio = libroServicio;
    }

    @GetMapping
    public ResponseEntity<?> obtenerLibros() {
        return ResponseEntity.ok(libroServicio.obtenerLibros());
    }

    @PostMapping
    public ResponseEntity<?> crearLibro(@Valid @RequestBody Libro libro){
        return ResponseEntity.ok(libroServicio.crearLibro(libro));
    }
}
