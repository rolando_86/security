package com.spring.security.spring.security.Entidad;

import com.spring.security.spring.security.Repositorio.UsuarioRepositorio;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UsuarioTest {

    @Autowired
    private UsuarioRepositorio usuarioRepositorio;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Test
    public void crearUsuarioTest() {

        Rol rol = new Rol();
        rol.setId(1L);
        rol.setNombreRol("ADMIN");

        Set<Rol> roles =  new HashSet<>();
        roles.add(rol);

        Usuario usuario = new Usuario();
        usuario.setId(1L);
        usuario.setNombreUsuario("admin");
        usuario.setContrasena(encoder.encode("admin"));
        usuario.setRoles(roles);
        Usuario retorno = usuarioRepositorio.save(usuario);
        //assertTrue(retorno.getContrasena().equals(usuario.getContrasena()));
    }

}