SPRING BOOT + SPRING SECURITY

GET
obtiene todos los libros
http://localhost:8080/api/libro

POST
crea un libro
http://localhost:8080/api/libro
ejemplo:

{
	"nombreLibro":"La cueva",
	"editorial":"LARAFEL",
	"cantidadHojas":200,
	"descripcion":"libro de terror, una historia basada en hechos reales",
	"habilitado":true
}

Crear una base llamada: security
Base de datos utilizada: MariaDB 10.4.10